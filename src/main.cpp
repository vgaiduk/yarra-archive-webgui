/*
 * Copyright (C) 2008 Emweb bvba, Heverlee, Belgium.
 *
 * See the LICENSE file for terms of use.
 */

#include <Wt/WApplication>
#include <Wt/WBreak>
#include <Wt/WContainerWidget>
#include <Wt/WLineEdit>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WTableView>
#include <Wt/WTable>
#include <Wt/WTableCell>
#include <Wt/WDialog>
#include <Wt/WLabel>

#include <Wt/WServer>

#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/backend/Sqlite3>

#include <Wt/Dbo/QueryModel>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include <signal.h>

//#include <functional>


//TODO: share the definition with the indexer
// A struct to be mapped to the db table
struct MeasFileMeta {

    //Time when an index entry has been seen or updated during the indexing
    std::time_t LastSeen;

    //The meas file attributes used in index indicating if the file shall be re-read
    std::string Filename;
    std::string Path;
    std::time_t LastWriteTime;

    //Meas file attributes read from the file tags
    std::string PatientName;
    std::string PatientID;
    std::string ProtocolName;
    std::string AcquisitionTime;
    std::string AcquisitionDate;
    std::string MRSystem;
    std::string AccessionNumber;
    std::string SelectedYarraServer;


    // Mapping method for Wt::Dbo
    template<class Action>
    void persist(Action &a) {
      Wt::Dbo::field(a, LastSeen, "LastSeen");

      Wt::Dbo::field(a, Filename, "Filename");
      Wt::Dbo::field(a, Path, "Path");
      Wt::Dbo::field(a, LastWriteTime, "LastWriteTime");

      Wt::Dbo::field(a, PatientName, "PatientName");
      Wt::Dbo::field(a, PatientID, "PatientID");
      Wt::Dbo::field(a, ProtocolName, "ProtocolName");

      Wt::Dbo::field(a, AcquisitionTime, "AcquisitionTime");
      Wt::Dbo::field(a, AcquisitionDate, "AcquisitionDate");
      Wt::Dbo::field(a, MRSystem, "MRSystem");
      Wt::Dbo::field(a, AccessionNumber, "AccessionNumber");
      Wt::Dbo::field(a, SelectedYarraServer, "SelectedYarraServer");
    }

    MeasFileMeta() : LastWriteTime(0), LastSeen(0) { }
};

//TODO: combine it with the indexer
//Read command line and config file options
bool readOptions(int argc, char *argv[], std::string & indexDB, std::string & config_file) {
    boost::program_options::options_description generic_options("Yarra Archive options");
    generic_options.add_options()
            ("index,I", boost::program_options::value<std::string>()->default_value(std::string(argv[0]) + ".db"),
             "index file name")
            ("passwd,P", boost::program_options::value<std::string>()->default_value(""), "user password")
            ("config,C", boost::program_options::value<std::string>(), "config file name");

    boost::program_options::options_description cmdline_options("Allowed options");
    cmdline_options.add_options()
            ("help,h", "produce help message");
    cmdline_options.add(generic_options);

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::command_line_parser(argc, argv)
                                          .options(cmdline_options).allow_unregistered().run(), vm);

    if (vm.count("help")) {

        std::cout << generic_options << std::endl
        << "The configuration file has INI-like line-based syntax." << std:: endl
        << "A line in the form:" << std:: endl
        << "    name=value" << std:: endl
        << "gives a value to an option." << std:: endl
        << "The # character introduces a comment that spans until the end of the line." << std:: endl
        << std:: endl
        << "Options are the same as listed above for the command line (used without --)." << std:: endl
        << std:: endl;
        return true;
    }

    config_file.erase();
    if (vm.count("config")) {
        config_file = vm["config"].as<std::string>();
        if (!boost::filesystem::is_regular_file(boost::filesystem::path(config_file))) {
            Wt::log("error") << "The config file `" << config_file << "` doesn't exist";
            return false;
        }
    } else {
        config_file = std::string(argv[0]) + ".ini";
        if (!boost::filesystem::is_regular_file(boost::filesystem::path(config_file))) {
            Wt::log("warn") << "Default config file `" << config_file << "` doesn't exist";
            config_file.erase();
        }
    }

    boost::program_options::options_description config_file_options("Allowed options");
    config_file_options.add(generic_options);

    if (!config_file.empty()) {
        try {
            boost::program_options::store(
                    boost::program_options::parse_config_file<char>(config_file.c_str(), config_file_options),
//                    boost::program_options::config_file_parser(config_file.c_str(), config_file_options)
//                            .options(cmdline_options).allow_unregistered().run(),
                    vm, true);
        } catch (const boost::program_options::error & e) {
            Wt::log("error") << "Error reading config file `" << config_file << "`: " << e.what();
            return false;
        }
    }

    boost::program_options::notify(vm);

    indexDB = vm["index"].as<std::string>();

    std::string passwd = vm["passwd"].as<std::string>();
    if (passwd.empty()) {
        Wt::log("error") << "The user password has not been set";
        return false;
    }
    return true;
}



using namespace Wt;

/*
 * A simple hello world application class which demonstrates how to react
 * to events, read input, and give feed-back.
 */
class HelloApplication : public WApplication
{
public:
    HelloApplication(const WEnvironment& env, const std::string & indexDB);

private:
    WLineEdit *searchEdit_;

    void applyFilter();

    void showDetails(const WModelIndex& item);

    void addDetailsRow(int row, const std::string & fieldName);

    Wt::Dbo::backend::Sqlite3 sqlite3;
    Wt::Dbo::Session session;
    Wt::Dbo::QueryModel< Wt::Dbo::ptr<MeasFileMeta> > *queryModel;
    WTableView *tableView;
    Wt::WTable *detailsTable;
    std::map<std::string, Wt::WText*> fieldMapping;
};

/*
 * The env argument contains information about the new session, and
 * the initial request. It must be passed to the WApplication
 * constructor so it is typically also an argument for your custom
 * application constructor.
*/
HelloApplication::HelloApplication(const WEnvironment& env, const std::string & indexDB)
    : WApplication(env), sqlite3(indexDB)
{
    setCssTheme("polished");
    useStyleSheet("styles.css");

    setTitle("Yarra Archive");                               // application title


    {
        Wt::WDialog *dialog = new Wt::WDialog("User password");

        Wt::WLabel *label = new Wt::WLabel("Password:", dialog->contents());
        Wt::WLineEdit *edit = new Wt::WLineEdit(dialog->contents());
        label->setBuddy(edit);


        Wt::WPushButton *ok = new Wt::WPushButton("OK", dialog->footer());
        ok->setDefault(true);
//        if (wApp->environment().ajax())
//            ok->disable();

        Wt::WPushButton *cancel = new Wt::WPushButton("Cancel", dialog->footer());
        dialog->rejectWhenEscapePressed();

        ok->clicked().connect(dialog, &Wt::WDialog::accept);
        cancel->clicked().connect(dialog, &Wt::WDialog::reject);

        std::string userPasswd;
        dialog->finished().connect(std::bind([=] () {
//            if (dialog->result() == Wt::WDialog::Accepted)
//                userPasswd = edit->text();
            delete dialog;
        }));

        dialog->show();
    }

    root()->addWidget(new WText("Search "));  // show some text
    searchEdit_ = new WLineEdit(root());                     // allow text input
    searchEdit_->setFocus();                                 // give focus

    WPushButton *button = new WPushButton("Apply", root());              // create a button
    button->setMargin(5, Left);                            // add 5 pixels margin

    root()->addWidget(new WBreak());                       // insert a line break

    button->clicked().connect(this, &HelloApplication::applyFilter);
    searchEdit_->enterPressed().connect(this, &HelloApplication::applyFilter);


    //Init the db connection
    session.setConnection(sqlite3);

    //Create table and db index
    session.mapClass<MeasFileMeta>("measFileIndex");

    queryModel = new Wt::Dbo::QueryModel< Wt::Dbo::ptr<MeasFileMeta> >();
    tableView = new WTableView(root());

    queryModel->setQuery(session.find<MeasFileMeta>());

    queryModel->addColumn("PatientName");
    queryModel->addColumn("PatientID");
    queryModel->addColumn("ProtocolName");
    queryModel->addColumn("AcquisitionDate");

    tableView->resize(1000, 600);
    tableView->setWidth(Wt::WLength("100%"));

    tableView->setSelectionMode(SingleSelection);
    tableView->setModel(queryModel);

    tableView->setAlternatingRowColors(true);

    tableView->setColumnWidth(0, 300);
    tableView->setColumnWidth(1, 150);
    tableView->setColumnWidth(2, 350);
    tableView->setColumnWidth(3, 150);

    tableView->sortByColumn(1, AscendingOrder);

    tableView->clicked().connect(this, &HelloApplication::showDetails);

    root()->addWidget(new WBreak());                       // insert a line break

    detailsTable = new Wt::WTable(root());
    detailsTable->setMargin(20);
    detailsTable->setHeaderCount(0);
    detailsTable->setWidth(Wt::WLength("100%"));

    int row = 0;

    addDetailsRow(row++, "Path");
    addDetailsRow(row++, "Filename");
//    addDetailsRow(row++, "LastWriteTime");
    addDetailsRow(row++, "Patient Name");
    addDetailsRow(row++, "Patient ID");
    addDetailsRow(row++, "Acquisition Date");
    addDetailsRow(row++, "Protocol Name");
    addDetailsRow(row++, "Acquisition Time");
    addDetailsRow(row++, "MR System");
    addDetailsRow(row++, "Accession Number");
    addDetailsRow(row++, "Selected Yarra Server");
}

void HelloApplication::addDetailsRow(int row, const std::string & fieldName) {
    Wt::WText* fieldValue = new Wt::WText();
    fieldMapping[fieldName] = fieldValue;

    detailsTable->elementAt(row, 0)->addWidget(new Wt::WText(fieldName));
    detailsTable->elementAt(row, 1)->addWidget(fieldValue);
}


void HelloApplication::showDetails(const WModelIndex& item) {

    auto meas = queryModel->stableResultRow(item.row());

    fieldMapping["Path"]->setText(meas->Path);
    fieldMapping["Filename"]->setText(meas->Filename);
    fieldMapping["Patient Name"]->setText(meas->PatientName);
    fieldMapping["Patient ID"]->setText(meas->PatientID);
    fieldMapping["Acquisition Date"]->setText(meas->AcquisitionDate);
    fieldMapping["Protocol Name"]->setText(meas->ProtocolName);
    fieldMapping["Acquisition Time"]->setText(meas->AcquisitionTime);
    fieldMapping["MR System"]->setText(meas->MRSystem);
    fieldMapping["Accession Number"]->setText(meas->AccessionNumber);
    fieldMapping["Selected Yarra Server"]->setText(meas->SelectedYarraServer);
}

void HelloApplication::applyFilter()
{
    std::stringstream filterStream(searchEdit_->text().toUTF8());
    searchEdit_->setSelection(0, filterStream.str().size());
    std::vector<std::string> filterVector;
    while (!filterStream.eof()) {
        std::string filter;
        filterStream >> filter;
        if (!filter.empty()) {
            filterVector.push_back(filter);
        }
    }

    if (filterVector.empty()) {
        queryModel->setQuery(session.find<MeasFileMeta>(), true);
    } else {
        auto query = session.query<Wt::Dbo::ptr<MeasFileMeta>>("select m from  measFileIndex m");
        for (auto filter : filterVector) {
            filter = "%" + filter + "%";
            query.where(
                    "m.Filename            LIKE ? OR "
                    "m.PatientName         LIKE ? OR "
                    "m.PatientID           LIKE ? OR "
                    "m.ProtocolName        LIKE ? OR "
                    "m.AcquisitionTime     LIKE ? OR "
                    "m.AcquisitionDate     LIKE ? OR "
                    "m.MRSystem            LIKE ? OR "
                    "m.AccessionNumber     LIKE ? OR "
                    "m.SelectedYarraServer LIKE ?")
                    .bind(filter)
                    .bind(filter)
                    .bind(filter)
                    .bind(filter)
                    .bind(filter)
                    .bind(filter)
                    .bind(filter)
                    .bind(filter)
                    .bind(filter);
        }
        queryModel->setQuery(query, true);
    }
}

//A copy from wt/src/http/WServer.C with a possibility to set the config file
int WRun(int argc, char *argv[], const std::string & config_file, ApplicationCreator createApplication)
{
    try {
        // use argv[0] as the application name to match a suitable entry
        // in the Wt configuration file, and use the default configuration
        // file (which defaults to /etc/wt/wt_config.xml unless the environment
        // variable WT_CONFIG_XML is set)
        WServer server(argv[0]);

        // WTHTTP_CONFIGURATION is e.g. "/etc/wt/wthttpd"
        server.setServerConfiguration(argc, argv, config_file);

        // add a single entry point, at the default location (as determined
        // by the server configuration's deploy-path)
        server.addEntryPoint(Wt::Application, createApplication);
        if (server.start()) {
            int sig = WServer::waitForShutdown(argv[0]);

            Wt::log("info") << "Shutdown (signal = " << sig << ")\n";
            server.stop();

            if (sig == SIGHUP)
                WServer::restart(argc, argv, environ);
        }
    } catch (WServer::Exception& e) {
        Wt::log("error") << e.what() << "\n";
        return 1;
    } catch (std::exception& e) {
        Wt::log("error") << "exception: " << e.what() << "\n";
        return 1;
    }
}

int main(int argc, char **argv)
{

    std::string indexDB;
    std::string config_file;
    if (!readOptions(argc, argv, indexDB, config_file)) {
        return 1;
    }

    return WRun(argc, argv, config_file.empty() ? WTHTTP_CONFIGURATION : config_file, [=](const WEnvironment& env)
    {
//        You could read information from the environment to decide whether
//        the user has permission to start a new application
        return new HelloApplication(env, indexDB);
    } );
}

